import React, { useEffect } from "react";
import { Container } from "reactstrap";
import { UserTable } from 'components/userTable'
import authStore from "./store"

export const Users = () => {
  const [authState, authActions] = authStore()

  useEffect(() => {
    authActions.getUsers()
  }, [authActions])

  return (
    <Container>
      <UserTable state={authState} />
    </Container>
  )
}