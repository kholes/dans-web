import ApiService from "services/ApiService";
export default class AuthService {
  static login = async (params) => {
    const instance = await ApiService();
    return instance.post("/login", {
      username: params.username,
      password: params.password,
    })
  }
}
