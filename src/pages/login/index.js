import React, { useState } from "react"
import {
  Form,
  FormGroup,
  Input,
  Container,
  Col,
  Button
} from "reactstrap"
import authStore from "./store"

export const Login = () => {
  const [, authActions] = authStore();
  const [username, setUsername] = useState(null)
  const [password, setPassword] = useState(null)

  const doLogin = () => {
    authActions.onSubmitLogin({username: username, password: password})
  };

  return (
      <Container>
        <Col sm="12" md={{ size: 10, offset: 1 }}>
          <Form>
            <FormGroup>
              <Input
                className="reset border"
                type="text"
                placeholder="Username"
                onChange={(e) => setUsername(e.target.value)}
              />
            </FormGroup>
            <FormGroup >
              <Input
                className="reset border"
                type="password"
                placeholder="Password"
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormGroup>
            <Button color="success" onClick={doLogin}>LOGIN</Button>
          </Form>
        </Col>
      </Container>
  );
}