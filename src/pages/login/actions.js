import AuthService from "./services";
import history from "routes/history";
// import { useHistory } from "react-router-dom"
export const onSubmitLogin = async (store, email, password, token) => {
  const isLoading = !store.state.isLoading;
  const status = 200;
  store.setState({ isLoading, status });
  AuthService.login(email, password, token)
    .then((response) => {
      const userData = response.data;
      localStorage.setItem("token", userData.token.access_token);
      localStorage.setItem("id", userData.id)
      history.push("/home");
    })
    .catch((error) => {
      console.log(error);
      const status = error.response ? error.response.status : "";
      const message = error.response ? error.response.data.message : "";
      const isLoading = false;
      store.setState({ isLoading, status, message });
    });
};
