import ApiService from "services/ApiService";
export default class AuthService {
  static getUsers = async (params) => {
    const instance = await ApiService();
    return instance.get("/users")
  }
}
