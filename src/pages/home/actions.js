import AuthService from "./services";

export const getUsers = async (store, email, password, token) => {
  const isLoading = !store.state.isLoading;
  const status = 200;
  store.setState({ isLoading, status });
  AuthService.getUsers(email, password, token)
    .then((response) => {
      const data = response.data
      store.setState({isLoading, data: data})
    })
    .catch((error) => {
      const status = error.response ? error.response.status : "";
      const message = error.response ? error.response.data.message : "";
      const isLoading = false;
      store.setState({ isLoading, status, message });
    });
};
