import ApiService from "services/ApiService";
export default class AuthService {
  static getJobs = async (filter) => {
    let filterParams = ''
        if (filter) {
            let arrayFilter = Object.keys(filter)
            let count = 0
            for(let i=0;i < arrayFilter.length;i++) {
                if (filter[arrayFilter[i]]) {
                    if (count === 0) {
                        filterParams += `?${arrayFilter[i]}=${filter[arrayFilter[i]]}`
                    } else {
                        filterParams += `&${arrayFilter[i]}=${filter[arrayFilter[i]]}`
                    }
                }
                count ++
            }
        }
    const instance = await ApiService();
    return instance.get(`/jobs${filterParams}`)
  }
}
