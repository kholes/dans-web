import React, { useEffect } from "react";
import { Container } from "reactstrap";
import { JobTable } from 'components/jobTable'
import { JobFilter } from 'components/jobFilter'
import authStore from "./store"

export const Jobs = () => {
  const [authState, authActions] = authStore()

  const getData = (filter) => {
    authActions.getJobs(filter)
  }

  useEffect(() => {
    authActions.getJobs()
  }, [authActions])

  return (
    <div>
      <Container>
        <JobFilter action={getData} />
      </Container>
      <Container>
        <JobTable state={authState} />
      </Container>
    </div>
  )
}