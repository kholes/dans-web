import AuthService from "./services";

export const getJobs = async (store, filter, token) => {
  const isLoading = !store.state.isLoading;
  const status = 200;
  store.setState({ isLoading, status });
  AuthService.getJobs(filter, token)
    .then((response) => {
      const data = response.data.data
      store.setState({isLoading, data: data})
    })
    .catch((error) => {
      const status = error.response ? error.response.status : "";
      const message = error.response ? error.response.data.message : "";
      const isLoading = false;
      store.setState({ isLoading, status, message });
    });
};
