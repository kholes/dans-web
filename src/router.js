import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import history from "routes/history";
import { Login } from "pages/login"
import { PrivateRoute } from "components/privateRoute";
import notFound from "pages/notFound";
import { clearLocalStorage } from "components/utils"
const token = localStorage.getItem("token")

function AppRouter() {
  return (
    <Router history={history}>
      <Switch>
        <Route exact name="Login" path="/" component={Login} />
        { token ? <PrivateRoute /> : clearLocalStorage() }
        <Route path="*" component={notFound} />
      </Switch>
    </Router>
  );
}

export default AppRouter;
