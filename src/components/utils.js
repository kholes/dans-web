import history from "routes/history"

export const clearLocalStorage = () => {
  localStorage.removeItem("token")
  history.push("/")
}
