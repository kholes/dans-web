import React, { useState } from 'react'
import { Form, FormGroup, Label, Input, Button } from 'reactstrap'
export const JobFilter = ({action}) => {
  const [description, setDescription] = useState(null)
  const [location, setLocation] = useState(null)

  const onSearch = () => {
    let filter = {}
    if (description){ filter.description = description }
    if (location){ filter.location = location }
    action(filter)
  }

  return (
    <Form style={{display: 'flex', alignItems: 'center', justifyContent: 'space-between'}}>
      <FormGroup>
        <Label for="description">
          Job Description
        </Label>
        <Input
          id="description"
          name="description"
          placeholder="Filter by title, companies, description"
          type="text"
          onChange={(e) => setDescription(e.target.value)}
          value={description}
        />
      </FormGroup>
      <FormGroup>
        <Label for="location">
          Location
        </Label>
        <Input
          id="location"
          name="location"
          placeholder="Filter by city, zipcode"
          type="text"
          onChange={(e) => setLocation(e.target.value)}
          value={location}
        />
      </FormGroup>
      <FormGroup check>
        <Input type="checkbox" />
        {' '}
        <Label check>
          Full time only
        </Label>
      </FormGroup>
      <Button style={{height: 40}} onClick={onSearch}>Search</Button>
    </Form>
  )
}