import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'

export const JobTable = ({ state }) => {
  const [jobs, setJobs] = useState([])
  const formatTime = (timeCurrent, timeCreated) => {
    let diff = timeCurrent - timeCreated;
    let lastStatement = ' ago'
    let periods = {
        month: 30 * 24 * 60 * 60 * 1000,
        week: 7 * 24 * 60 * 60 * 1000,
        day: 24 * 60 * 60 * 1000,
        hour: 60 * 60 * 1000,
        minute: 60 * 1000
    }
    
    if (diff > periods.month) {
      return Math.floor(diff / periods.month) + " month" + lastStatement;
    } else if (diff > periods.week) {
      return Math.floor(diff / periods.week) + " week" + lastStatement;
    } else if (diff > periods.day) {
      return Math.floor(diff / periods.day) + " day" + lastStatement;
    } else if (diff > periods.hour) {
      return Math.floor(diff / periods.hour) + " hours" + lastStatement;
    } else if (diff > periods.minute) {
      return Math.floor(diff / periods.minute) + " minute" + lastStatement;
    }
    return "Baru saja";
  }

  const currentDate = () => {
    let date = new Date().getDate()
    let month = new Date().getMonth() + 1
    let year = new Date().getFullYear()
    let hours = new Date().getHours()
    let min = new Date().getMinutes()
    let sec = new Date().getSeconds()

    return year + '-' + month + '-' + date + ' ' + hours + ':' + min + ':' + sec
  }

  useEffect(() => {
    if (state.data) {
      setJobs(state.data)
    }
  }, [state])

  return (
    <div>
      <h3>Job List</h3>
      {
        jobs.length > 0 && 
        jobs.map((value, key) => {
        return (
          <Link key={key} className="list-container" to={{pathname: '/job-detail', job: value}}>
            <div>
              <h3>{value.name}</h3>
              <div>
                <span>{value.company}</span>
                <span className="long"> - </span>
                <span className="blue bold">{value.status}</span>
              </div>
            </div>
            <div>
              <p style={{padding: 0, margin: 0, color: '#999', fontSize: 12}}>{value.location}</p>
              <p style={{padding: 0, margin: 0, color: '#999', fontSize: 12}}>
                {formatTime(moment(currentDate(), "YYYY-M-D H:mm:ss"), moment(value.created_at, "YYYY-M-D H:mm:ss"))}
              </p>
            </div>
          </Link>
        )})
      }
    </div>
  )
}