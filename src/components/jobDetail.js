import React from 'react'
import { useHistory } from 'react-router-dom'
import { Container } from 'reactstrap'

export const JobDetail = (props) => {
  const history = useHistory()
  return (
    <Container>
      <h3>Job Detail</h3>
      <button onClick={history.goBack}>Back</button>
      <div>
        {
          JSON.stringify(props.location.job)
        }
      </div>
    </Container>
  )
}