import React from 'react'
import { Route } from 'react-router-dom'
import { Home } from "pages/home"
import { Users } from 'pages/users'
import { Jobs } from 'pages/jobs'
import { JobDetail } from 'components/jobDetail'

import { NavBar } from 'components/NavBar'

export const PrivateRoute = () => {
  return (
      <>
        <NavBar />
        <Route exact name="Home" path="/home" component={Home} />
        <Route exact name="Users" path="/users" component={Users} />
        <Route exact name="Jobs" path="/jobs" component={Jobs} />
        <Route exact name="JobDetail" path="/job-detail" component={JobDetail} />
      </>
  )
}