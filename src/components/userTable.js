import React, { useEffect, useState } from 'react'
import { Table } from 'reactstrap'

export const UserTable = ({ state }) => {
  const [users, setUsers] = useState([])

  useEffect(() => {
    if (state.data) {
      setUsers(state.data)
    }
  }, [state])

  return (
    <div>
      <h3>User List</h3>
      <Table>
        <thead>
          <tr>
            <th>No</th>
            <th>Username</th>
            <th>Password</th>
          </tr>
        </thead>
        <tbody>
          {
            users.length > 0 && 
            users.map((value, key) => {
              return (
                <tr key={key}>
                  <th scope="row">{key + 1}</th>
                  <td>{value.username}</td>
                  <td>{value.password}</td>
                </tr>
              )
            })
          }
          </tbody>
      </Table>
    </div>
  )
}