import Axios from "axios";
const defaultHeader = async () => {
  const token = localStorage.getItem("token");
  return {
    Accept: "application/json",
    "Content-Type": "application/json",
    Authorization: token ? `Bearer ${token}` : `Bearer 12345`,
    "X-App-Group": "1",
    "X-App-Brand": "1",
    "X-App-Source": "web",
    "X-App-Version": "0.0.1",
    "X-App-Os": "Chrome",
    "X-App-Language": "en",
  };
};

const ApiService = async () => {
  const header = await defaultHeader();

  const instance = Axios.create({
    baseURL: process.env.REACT_APP_API_URL,
    timeout: 60000,
    headers: header,
  });

  return instance;
};

export default ApiService;
